ARG python_version=3.10
ARG python_index_url=https://pypi.org/simple

FROM python:$python_version AS builder
ARG python_version
ARG python_index_url
WORKDIR /app
ENV PYTHONUSERBASE=/opt/python
ENV PATH=/opt/python/bin:/usr/local/bin:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin
ENV PIP_INDEX_URL=$python_index_url
RUN pip install --upgrade pdm
# Manually copy pdm files to keep cache efficient
COPY pyproject.toml pdm.lock /app/
RUN pdm export --prod --output=requirements.txt
RUN pip install --user --ignore-installed --requirement=requirements.txt
# Now copy full application for installation
COPY . /app
RUN pip install --user --no-deps .

FROM python:$python_version-slim
ARG python_version
ENV PYTHONUSERBASE=/opt/python
ENV PATH=/opt/python/bin:/usr/local/bin:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin
RUN apt -y update && apt install -y --no-install-recommends nfs-common bindfs && rm -rf /var/lib/apt/lists/*
COPY --from=builder /opt /opt
# For trivy container scanning
COPY --from=builder /app/requirements.txt /requirements.txt
ENTRYPOINT ["python", "-m", "csi_plugin_nfs"]
