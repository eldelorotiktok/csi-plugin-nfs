NFS CSI-Plugin
==============

This CSI plugin provides a simple container orchestrator independent way to mount folders from an NFS share. Hashicorp's Nomad is the main target for this plugin and as such it might not provide a full experience on other orchestrators (please open an issue if that is actually the case).

The plugin operates without an extra state-store and tries to be as simple as possible. It requires one NFS share where it will create subfolders as needed which can then be mounted to containers (file-system attachments only, no support for block devices currently).

The plugin is tested using [csi-sanity](https://github.com/kubernetes-csi/csi-test/tree/master/cmd/csi-sanity) against a userspace NFS-server.

Configuration
-------------

Ensure that you have an NFS-share that is mountable from the nodes where you'd like to use this plugin. Please be aware that this plugin uses subfolders on a single NFS-share and assumes full control of the path (ie it might try to delete a folder as response to a `DeleteVolume` command).

To run the plugin execute the following (we are using docker/podman here because that is how it will be run in a container orchestrator anways):

```
docker run --privileged registry.gitlab.com/rocketduck/csi-plugin-nfs:latest \
    --type=monolith --endpoint=unix:///csi/csi.sock --node-id=your_hostname \
    --nfs-server=nfs_server:/share --mount-options=defaults
```

The options do the following:

 * `--type` can be `monolith` to host the node and controller parts inside a single process or `node|controller` in which case you will have to start multiple instances (nodes and controllers do not communicate directly though). The latter is the prefered way of deploying since generally only one controller is needed.
 * `--endpoint` specifies the endpoint for the CSI command socket, use `localhost:8888` to expose a TCP-socket instead.
 * `--node-id` should be the id (hostname) of the node you are running on. This is required by the CSI spec.
 * `--nfs-server` specifies the NFS share to use; it (and subsequently subfolders) will be mounted using `--mount-options`


Deployment in Nomad
-------------------

Please see the `nomad` subfolder in this repository for details about Nomad specific things (like job files).

Roadmap
-------

 * Maybe support cloning and snapshots, even if the will not be atomic.
 * Support for block-devices, although it is not exactly clear which benefit they would provide (Please open an issue if we miss something here).
 * Better support for `csi-sanity` where possible. The plugin currently passes the `csi-sanity` testsuite but skips at least one test that is hard (to impossible) to implement without storing state about the created volumes.
 * Support multiple NFS servers as targets so the user can choose a "storage class" (ideas welcome).