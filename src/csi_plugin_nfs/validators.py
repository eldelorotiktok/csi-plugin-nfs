import re

import grpc

VOLUME_NAME_RE = re.compile("^[a-zA-Z0-9_-]+$")


class RequiredField:
    def __init__(self, *fields):
        self.fields = fields

    def __call__(self, service, request, context):
        for attr in self.fields:
            try:
                has_field = request.HasField(attr)
            except ValueError:
                has_field = hasattr(request, attr)
            if not has_field or not getattr(request, attr):
                msg = f"{attr} is required"
                service.logger.debug(msg)
                context.abort(grpc.StatusCode.INVALID_ARGUMENT, msg)


class VolumeNameValidator:
    def __init__(self, name):
        self.name = name

    def __call__(self, service, request, context):
        name = getattr(request, self.name)
        if not VOLUME_NAME_RE.fullmatch(name):
            service.logger.debug(
                f"Requested volume name containes invalid characters: {name!r}"
            )
            context.abort(
                grpc.StatusCode.INVALID_ARGUMENT,
                "invalid volume name",
            )


def validate(*validators):
    def outer(func):
        def inner(self, request, context):
            for validator in validators:
                validator(self, request, context)
            return func(self, request, context)

        return inner

    return outer
