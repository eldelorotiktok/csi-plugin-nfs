import os

from csi_plugin_nfs import csi_pb2, csi_pb2_grpc

from . import utils
from .validators import RequiredField, VolumeNameValidator, validate


class NodeServicer(utils.CSIServiceMixin, csi_pb2_grpc.NodeServicer):
    def NodeGetCapabilities(self, request, context):
        return csi_pb2.NodeGetCapabilitiesResponse()

    def NodeGetInfo(self, request, context):
        return csi_pb2.NodeGetInfoResponse(
            node_id=self.config.node_id, max_volumes_per_node=0
        )

    @validate(
        RequiredField("volume_id", "target_path", "volume_capability"),
        VolumeNameValidator("volume_id"),
    )
    def NodePublishVolume(self, request, context):
        self.logger.info(
            f"Received mount request for {request.volume_id!r} at {request.target_path!r}"
        )

        # Autocreate the volume since nomad does not support Create/Delete yet.
        if not self.backend.exists(request.volume_id):
            self.backend.create(request.volume_id, request.volume_context)

        try:
            os.mkdir(request.target_path)
        except FileExistsError:
            already_mounted, options = utils.check_mount(request.target_path)
            readonly = "ro" in options
            if already_mounted and readonly == request.readonly:
                return csi_pb2.NodePublishVolumeResponse()
            self.set_grpc_error("NodePublishVolume/mkdir", context)
            raise

        try:
            utils.mount(
                self.config,
                request.volume_id,
                request.target_path,
                read_only=request.readonly,
            )
        except Exception:
            self.set_grpc_error("NodePublishVolume/mount", context)
            try:
                os.rmdir(request.target_path)
            except FileNotFoundError:
                pass
            raise

        return csi_pb2.NodePublishVolumeResponse()

    @validate(
        RequiredField("volume_id", "target_path"),
        VolumeNameValidator("volume_id"),
    )
    def NodeUnpublishVolume(self, request, context):
        self.logger.info(
            f"Received unmount request for {request.volume_id} at {request.target_path}"
        )

        if not os.path.exists(request.target_path):
            self.logger.warn(
                f"Target path {request.target_path} does not exist for {request.volume_id}"
            )
            return csi_pb2.NodeUnpublishVolumeResponse()

        try:
            utils.umount(request.target_path)
        except Exception:
            self.set_grpc_error("NodeUnpublishVolume/umount", context)
            raise

        try:
            os.rmdir(request.target_path)
        except Exception:
            self.set_grpc_error("NodeUnpublishVolume/rmdir", context)
            raise

        return csi_pb2.NodeUnpublishVolumeResponse()
