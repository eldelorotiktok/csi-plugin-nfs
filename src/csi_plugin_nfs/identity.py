from importlib.metadata import version

from csi_plugin_nfs import csi_pb2, csi_pb2_grpc

from . import utils


class IdentityServicer(utils.CSIServiceMixin, csi_pb2_grpc.IdentityServicer):
    def GetPluginInfo(self, request, context):
        return csi_pb2.GetPluginInfoResponse(
            name="dev.rocketduck.csi.nfs",
            vendor_version=version("csi_plugin_nfs"),
        )

    def GetPluginCapabilities(self, request, context):
        Cap = csi_pb2.PluginCapability
        Service = Cap.Service
        Type = Service.Type
        return csi_pb2.GetPluginCapabilitiesResponse(
            capabilities=[Cap(service=Service(type=Type.CONTROLLER_SERVICE))]
        )

    def Probe(self, request, context):
        return csi_pb2.ProbeResponse()
