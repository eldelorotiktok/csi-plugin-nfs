import os
import subprocess
import tempfile
import weakref
from pathlib import Path
from typing import Dict

from . import utils


class StorageBackend:
    def __init__(self, config):
        self.config = config
        self.storage_root = Path(tempfile.mkdtemp())
        utils.mount(config, None, self.storage_root)
        weakref.finalize(self, self._cleanup)

    def _cleanup(self):
        utils.umount(self.storage_root)
        self.storage_root.rmdir()

    def _safe_storage_path(self, path: str) -> Path:
        if ".." in path or "/" in path:
            raise ValueError("Path injection tried?")
        return self.storage_root / path

    def volume_id_from_name(self, name: str) -> str:
        return name

    def create(self, name: str, parameters: Dict[str, str]) -> str:
        volume_id = self.volume_id_from_name(name)
        final_path = self._safe_storage_path(volume_id)
        final_path.mkdir(int(parameters.get("mode", "777"), 8), exist_ok=True)
        os.chown(
            final_path,
            int(parameters.get("uid", "-1")),
            int(parameters.get("gid", "-1")),
        )
        return volume_id

    def delete(self, volume_id: str) -> None:
        final_path = self._safe_storage_path(volume_id)
        if final_path.exists():
            subprocess.check_call(["rm", "-rf", final_path])

    def exists(self, volume_id: str) -> bool:
        final_path = self._safe_storage_path(volume_id)
        return final_path.exists() and final_path.is_dir()
