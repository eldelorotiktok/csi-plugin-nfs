v0.4.0 (23.05.2022)
-------------------

 * Dependency updates and internal restructuring.

v0.3.0 (22.05.2021)
-------------------

 * Properly implemented `ValidateVolumeCapabilities`.
 * Added request/response logging when the log level is DEBUG.

v0.2.0 (02.01.2021)
-------------------

 * Multi-Arch docker images, linux/amd64 and linux/arm64 for now (#2).
 * Support uid/gid/mode during volume creation (#1).

v0.1.0 (01.01.2021)
-------------------

 * Initial release, not much to say :)
